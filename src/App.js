import React, { useRef, useEffect, useState } from "react";
import "./App.css";
import { select, axisBottom, axisRight, scaleLinear, scaleBand, curveCardinal, line } from "d3";

function App() { 
  //Datasets for the Bar Chart
  const [data1, setData1] = useState([25, 30, 45, 60, 10, 65, 75]);
  //Datasets for the Line Chart 
  const [data2, setData2] = useState([12, 23, 29, 37, 39, 41, 47]);
  const svgRef1 = useRef();
  const svgRef2 = useRef();

  // on increase or decrease of Bar Chart this will be called
  useEffect(() => {
    const svg1 = select(svgRef1.current);
    const xScale = scaleBand()
      .domain(data1.map((value, index) => index))
      .range([0, 300])
      .padding(0.5);

    const yScale = scaleLinear()
      .domain([0, 150])
      .range([150, 0]);

    const colorScale = scaleLinear()
      .domain([75, 100, 150])
      .range(["green", "orange", "red"])
      .clamp(true);

    const xAxis = axisBottom(xScale).ticks(data1.length);

    svg1
      .select(".x-axis")
      .style("transform", "translateY(150px)")
      .call(xAxis);

    const yAxis = axisRight(yScale);
    svg1
      .select(".y-axis")
      .style("transform", "translateX(300px)")
      .call(yAxis);

    svg1
      .selectAll(".bar")
      .data(data1)
      .join("rect")
      .attr("class", "bar")

      .style("transform", "scale(1, -1)")
      .attr("x", (value, index) => xScale(index))
      .attr("y", -150)
      .attr("width", xScale.bandwidth())
      .transition()
      .attr("fill", colorScale)
      .attr("height", value => 150 - yScale(value));
  }, [data1]);

  // on increase or decrease of Line Chart this will be called
  useEffect(() => {
    const svg2 = select(svgRef2.current);
    const lineChart = line()
    .x((value, index) => index * 50)
    .y(value => 150 - value)
    .curve(curveCardinal);
    
    svg2
        .selectAll("path")
        .data([data2])
        .join("path")
        .attr("d", value => lineChart(value))
        .attr("fill", "none")
        .attr("stroke", "blue");
  }, [data2]);

  return (
    <React.Fragment>
      <h2>Implementation Of Charts Using D3</h2>
      <svg ref={svgRef1}>
        <g className="x-axis" />
        <g className="y-axis" />
      </svg>
      <button onClick={() => setData1(data1.map(value => value + 10))}>
        Increase Bar Chart
      </button>
      <button onClick={() => setData1(data1.map(value => value - 10))}>
        Decrease Bar Chart
      </button>
      <svg ref={svgRef2}></svg>
      <button onClick={() => setData2(data2.map(value => value + 10))}>
        Increase Line Chart
      </button>
      <button onClick={() => setData2(data2.map(value => value - 10))}>
        Decrease Line Chart
      </button>
    </React.Fragment>
  );
}

export default App;